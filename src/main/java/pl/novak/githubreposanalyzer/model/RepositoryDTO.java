package pl.novak.githubreposanalyzer.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
@Builder
public class RepositoryDTO {
    private String name;
    private String owner;
    private List<BranchDTO> branches;

    public RepositoryDTO(RepositoryModel model) {
        this.name = model.getName();
        this.owner = model.getOwner().getLogin();
    }
}
