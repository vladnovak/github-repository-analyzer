package pl.novak.githubreposanalyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;

@Builder
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class BranchDTO {
    private String name;
    private String lastCommitSha;

    public BranchDTO(BranchModel model) {
        this.name = model.getName();
        this.lastCommitSha = model.getCommit().getSha();
    }
}
