package pl.novak.githubreposanalyzer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.novak.githubreposanalyzer.client.RepositoryInfoClient;
import pl.novak.githubreposanalyzer.exception.ErrorResponse;
import pl.novak.githubreposanalyzer.model.RepositoryDTO;

@RestController
@RequestMapping("/api/v1/repositories")
@RequiredArgsConstructor
@Tag(name = "Analyzer Controller", description = "Methods for GitHub information retrieving")
public class AnalyzerController {
    private final RepositoryInfoClient client;
    @Operation(summary = "Get selected details of all non forked repositories owned by a given GitHub user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Collection of non forked repo details for a given user",
            content = {@Content(mediaType = "application/json",
            schema = @Schema(implementation = RepositoryDTO.class))}),
            @ApiResponse(responseCode = "404", description = "GitHub user with the provided username is not existing",
            content = {@Content(mediaType = "application/json",
            schema = @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "406", description = "Response for request with unsupported media type in Accept header",
            content = {@Content(schema = @Schema(implementation = String.class))})
    })
    @GetMapping(value = "/{username}",
            produces = "application/json")
    public ResponseEntity findAllNonForkReposByUsername(
            @PathVariable
            @Parameter(description = "GitHub user's username")
            String username) {
        return ResponseEntity.ok(client.fetchReposByUsername(username));
    }
}
