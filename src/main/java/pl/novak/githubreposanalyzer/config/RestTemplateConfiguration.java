package pl.novak.githubreposanalyzer.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import pl.novak.githubreposanalyzer.exception.RestTemplateErrorHandler;

@Configuration
@RequiredArgsConstructor
public class RestTemplateConfiguration {
    private final RestTemplateErrorHandler restTemplateErrorHandler;
    @Bean
    public RestTemplate configureRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .errorHandler(restTemplateErrorHandler)
                .build();
    }

}
