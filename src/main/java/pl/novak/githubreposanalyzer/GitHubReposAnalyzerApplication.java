package pl.novak.githubreposanalyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitHubReposAnalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitHubReposAnalyzerApplication.class, args);
	}

}
