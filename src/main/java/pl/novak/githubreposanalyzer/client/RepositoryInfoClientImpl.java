package pl.novak.githubreposanalyzer.client;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.novak.githubreposanalyzer.exception.CustomException;
import pl.novak.githubreposanalyzer.model.BranchDTO;
import pl.novak.githubreposanalyzer.model.BranchModel;
import pl.novak.githubreposanalyzer.model.RepositoryDTO;
import pl.novak.githubreposanalyzer.model.RepositoryModel;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RepositoryInfoClientImpl implements RepositoryInfoClient {
    private final RestTemplate restTemplate;
    @Value("${github_api.host}")
    private String host;
    @Value("${github_api.scheme}")
    private String scheme;

    @Override
    public List<RepositoryDTO> fetchReposByUsername(String username) {

        var urlRepos = UriComponentsBuilder.newInstance()
                .scheme(scheme)
                .host(host)
                .path("/users/{username}/repos")
                .buildAndExpand(username)
                .toUriString();

        var responseBody = restTemplate
                .exchange(urlRepos, HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<RepositoryModel>>() {
                        })
                .getBody();

        if (responseBody != null) {
            var repositoryDTOs = responseBody
                    .stream()
                    .filter(repositoryModel -> !repositoryModel.isFork())
                    .map(RepositoryDTO::new)
                    .toList();

            repositoryDTOs
                    .forEach(repositoryDTO
                            -> repositoryDTO.setBranches(getBranchDTOsForRepository(repositoryDTO)));

            return repositoryDTOs;
        } else
            throw new CustomException("GitHub API returned null body for the request with the provided username",
                    HttpStatus.BAD_REQUEST);
    }

    private List<BranchDTO> getBranchDTOsForRepository(RepositoryDTO repositoryDTO) {
        var urlBranches = UriComponentsBuilder.newInstance()
                .scheme(scheme)
                .host(host)
                .path("/repos/{owner}/{name}/branches")
                .buildAndExpand(repositoryDTO.getOwner(), repositoryDTO.getName())
                .toUriString();

        var responseBody = restTemplate
                .exchange(urlBranches, HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<BranchModel>>() {
                        })
                .getBody();

        if (responseBody != null) {
            return responseBody
                    .stream()
                    .map(BranchDTO::new)
                    .collect(Collectors.toList());
        } else
            throw new CustomException("GitHub API returned null body for the request with provided owner and repo name",
                    HttpStatus.BAD_REQUEST);
    }
}
