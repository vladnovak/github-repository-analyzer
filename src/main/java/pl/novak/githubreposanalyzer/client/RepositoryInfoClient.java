package pl.novak.githubreposanalyzer.client;

import pl.novak.githubreposanalyzer.model.RepositoryDTO;

import java.util.List;

public interface RepositoryInfoClient {
    List<RepositoryDTO> fetchReposByUsername(String username);
}
