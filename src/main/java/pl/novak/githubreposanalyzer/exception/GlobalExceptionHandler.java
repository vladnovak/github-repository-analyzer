package pl.novak.githubreposanalyzer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(CustomException.class)
    ResponseEntity handleUserNotFound(CustomException ex) {
        return ResponseEntity
                .status(ex.getHttpStatus())
                .body(new ErrorResponse(ex.getHttpStatus(), ex.getMessage()));
    }

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    ResponseEntity handleMediaTypeNotAcceptable() {
        return ResponseEntity
                .status(HttpStatus.NOT_ACCEPTABLE)
                .body("Received request with not supported expected response format. Please retry with \"application/json\" in \"Accept\" header.");
    }
}
