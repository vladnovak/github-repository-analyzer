package pl.novak.githubreposanalyzer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Component
public class RestTemplateErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return response.getStatusCode().is4xxClientError() || response.getStatusCode().is5xxServerError();
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        var receivedStatus = HttpStatus.valueOf(response.getStatusCode().value());
        if (receivedStatus.is4xxClientError()) {
            throw new CustomException(
                    String.format("External API returned status %s. Please make sure your request query parameters are correct.",
                            receivedStatus), receivedStatus);
        } else if (receivedStatus.is5xxServerError()) {
            throw new CustomException(String.format("External API returned server error with status %s. Please contact support.", receivedStatus),
                    receivedStatus);
        }
    }
}
