package pl.novak.githubreposanalyzer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.novak.githubreposanalyzer.exception.ErrorResponse;
import pl.novak.githubreposanalyzer.model.BranchDTO;
import pl.novak.githubreposanalyzer.model.RepositoryDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static java.lang.String.join;
import static java.nio.file.Files.readAllLines;
import static java.util.List.of;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WireMockTest(httpPort = 8080)
@ActiveProfiles("test")
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@DisplayName("AnalyzerController#findAllNonForkReposByUsername ")
class AnalyzerControllerTest {
    @Autowired
    AnalyzerController analyzerController;
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;
    private static final String REPOS_FOR_USERNAME_PATH = "/api/v1/repositories/%s";

    @DisplayName("should return status OK with response body for an existing github user.")
    @Test
    void findAllNonForkReposByUsernameShouldReturnStatusOkWithBody() throws Exception {
        var expectedListOfRepoDTO = of(RepositoryDTO.builder()
                .name("testTaskForJavarushInternship")
                .owner("vladyslavnovak")
                .branches(of(BranchDTO.builder()
                        .name("main")
                        .lastCommitSha("aa0785c3abfc9e8b79c4beed6d5ab5d2c3f77d25")
                        .build()))
                .build());

        stubFor(WireMock.get("/users/vladyslavnovak/repos")
                .willReturn(okJson(readJsonFromFileByName("responseWithRepos"))));

        stubFor(WireMock.get("/repos/vladyslavnovak/testTaskForJavarushInternship/branches")
                .willReturn(okJson(readJsonFromFileByName("responseWithBranches"))));

        mockMvc.perform(MockMvcRequestBuilders.get(REPOS_FOR_USERNAME_PATH.formatted("vladyslavnovak")))
                .andExpectAll(
                        status().isOk(),
                        content().contentType(MediaType.APPLICATION_JSON),
                        content().json(objectMapper.writeValueAsString(expectedListOfRepoDTO)));
    }

    private static String readJsonFromFileByName(String name) throws URISyntaxException, IOException {
        return join("", readAllLines(Paths.get(AnalyzerControllerTest.class.getClassLoader()
                .getResource("testJson/%s.json".formatted(name)).toURI())));
    }

    @Test
    @DisplayName("should return status 406 with message for requests with unsupported expected response format.")
    void findAllNonForkReposByUsernameShouldReturnStatus406() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(REPOS_FOR_USERNAME_PATH.formatted("vladyslavnovak"))
                        .accept(MediaType.APPLICATION_XML))
                .andExpectAll(
                        status().isNotAcceptable(),
                        content().string("Received request with not supported expected response format. Please retry with \"application/json\" in \"Accept\" header.")
                );
    }

    @DisplayName(" should return error status with message in case of 4xx or 5xx error at external API.")
    @ParameterizedTest(name = "case #{index} - external API should return Http status {1}")
    @MethodSource("provideArgsForExceptionTest")
    void findAllNonForkReposByUsernameShouldReturnError(
            String json, int status, ErrorResponse expectedError) throws Exception {

        stubFor(WireMock.get("/users/somenotexistingusernamesdafasdfas/repos")
                .willReturn(jsonResponse(json, status)));

        mockMvc.perform(MockMvcRequestBuilders.get(REPOS_FOR_USERNAME_PATH.formatted("somenotexistingusernamesdafasdfas")))
                .andExpectAll(
                        status().is(status),
                        content().json(objectMapper.writeValueAsString(expectedError)));
    }

    private static Stream<Arguments> provideArgsForExceptionTest() throws URISyntaxException, IOException {
        return Stream.of(
                Arguments.of("{}", 505, new ErrorResponse(HttpStatus.HTTP_VERSION_NOT_SUPPORTED,
                        "External API returned server error with status 505 HTTP_VERSION_NOT_SUPPORTED. Please contact support.")),
                Arguments.of(readJsonFromFileByName("responseNotFound"), 404, new ErrorResponse(HttpStatus.NOT_FOUND,
                        "External API returned status 404 NOT_FOUND. Please make sure your request query parameters are correct."))
        );
    }
}