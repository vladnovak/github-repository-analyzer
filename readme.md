# GitHub Repository Analyzer

### Description
This repository contains my solution to a test task for a junior Java developer position. 
The task description and technical requirements can be found in the [Task Description PDF](task_description.pdf).</br>

The application acts as a RESTful API endpoint, employing RestTemplate to communicate with the GitHub API. Upon receiving a GET request, the controller processes the query, interacts with the GitHub API, and provides details about projects for a specified public GitHub profile.

Developed using **Java 17** and **Spring Boot 3**,
the application has been tested using the **Wiremock** framework, achieving over 90% coverage.

### System requirements
* Java 17 or higher 
* Maven 3.1 or higher
* Internet connection in order to make calls to GitHub API
* Docker (optional)

### How to Run
There are two options to run the project:
1. Run it as a Maven project from the terminal.</br> Switch to the project directory and execute the following command:
```shell
mvn spring-boot:run
```

2. Run it in a Docker container.</br>
   To run a container with the project, run the following command: 
```shell
docker run -dp 127.0.0.1:8080:8080 vla9d5/github-analyzer
```
Once started, the application will be accessible on the host via port 8080 at ```/githubrepoanalyzer```.</br>

In order to fetch information about all non-forked repositories of a given GitHub user, send a GET request to: 

```yourhost/githubrepoanalyzer/api/v1/repositories/{username}```

If the user with the provided username exists, a response in JSON format will be returned. For more details about possible responses and its structure please check API documentation. 

### Information for developers
* API documentation is accessible at ```/githubrepoanalyzer/api-docs```
* Swagger UI is available at ```/githubrepoanalyzer/swagger```
* GitHub API scheme and host can be modified using environment variables GITHUB_SCHEME and GITHUB_HOST, current default values are 'https' and 'api.github.com'

### Contact
Feel free to [email me](mailto:vla9d5@gmail.com) with any questions or suggestions regarding this project.
